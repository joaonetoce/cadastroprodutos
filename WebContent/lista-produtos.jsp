<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*,
br.com.fic.dao.*,
br.com.fic.modelo.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lista Produtos</title>
</head>
<body>
	<h2>Lista de Produtos</h2>
	<table>
		<%
			ProdutoDAO dao = new ProdutoDAO();
			List<Produto> produtos = dao.getLista();
		
		for(Produto produto : produtos){			
		%>
		<tr>
			<td><%=produto.getCodigo() %> <label> - </label></td>
			<td><%=produto.getDescricao() %> <label>: </label></td>
			<td><label> R$ </label> <%=produto.getPreco() %></td>
		</tr>
		<% 
		}
		%>
	</table>
	<br></br>
	<a href="/cadastroprodutos/bemvindo.jsp" name="MenuPrincipal">Menu Principal</a>
</body>
</html>