<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Menu Principal</title>
</head>
<body>
	<h1>Menu Principal</h1>
	<br></br>
	<a href="/cadastroprodutos/lista-produtos.jsp" name="ListaProdutos">Lista
		de Produtos</a> |
	<a href="/cadastroprodutos/adiciona-produto.html"
		name="AdicionaProduto">Adicionar Novo Produto</a> |
	<a href="/cadastroprodutos/altera-produto.jsp" name="AlterarProduto">Alterar
		Produto</a> |
	<a href="/cadastroprodutos/remove-produto.jsp" name="RemoverProduto">Remover
		Produto</a>

</body>
</html>