<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="java.util.*,
br.com.fic.dao.*,
br.com.fic.modelo.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Remove Produto</title>
</head>
<body>
 <h1>Remover Produto</h1>
 	<table>
		<%
			ProdutoDAO dao = new ProdutoDAO();
			List<Produto> produtos = dao.getLista();
		
		for(Produto produto : produtos){			
		%>
		<tr>
			<td><%=produto.getCodigo() %> <label> - </label></td>
			<td><%=produto.getDescricao() %> <label>: </label></td>
			<td><label> R$ </label> <%=produto.getPreco() %></td>
		</tr>
		<% 
		}
		%>
	</table>
	<form action="controle">
		<label>ID: </label> <input type="text" name="codigo" /><br />
		<input type="hidden" name="tipoRequisicao" value="remove" />
		<input type="submit" value="Excluir" />
	</form>
 	<br></br>
	<a href="/cadastroprodutos/bemvindo.jsp" name="MenuPrincipal">Menu Principal</a>
</body>
</html>