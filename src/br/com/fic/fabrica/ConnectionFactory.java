package br.com.fic.fabrica;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

	public Connection getConnection() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			return DriverManager.getConnection(
					"jdbc:mysql://localhost/produtosdb", "root", "123456");
			
		} catch (SQLException e) {
			Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
			String dbURL = "jdbc:derby://localhost:1527/produtosdb;create=false;user=root;password=123456";
			
			return DriverManager.getConnection(dbURL);
		}
	}
}
