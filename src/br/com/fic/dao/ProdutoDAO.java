package br.com.fic.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.fic.fabrica.ConnectionFactory;
import br.com.fic.modelo.Produto;

public class ProdutoDAO {

private Connection connection;
	
	public ProdutoDAO() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
		this.connection = new ConnectionFactory().getConnection();
	}
	
	public void adiciona(Produto produto){
		String  sql = "insert into produtos (descricao,preco) values (?,?)";
		
		try {
			PreparedStatement stmt = this.connection.prepareStatement(sql);
			
			stmt.setString(1, produto.getDescricao());
			stmt.setDouble(2, produto.getPreco());
			
			stmt.execute();
			stmt.close();
		} catch (Exception e) {
			throw new RuntimeException(e); 
		}
	}
	
	public List<Produto> getLista(){
		try {
			List<Produto> contatos = new ArrayList<>();
			PreparedStatement stmt = this.connection.prepareStatement("select * from produtos");
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next()){
				Produto contato = new Produto();
				contato.setCodigo(rs.getLong("codigo"));
				contato.setDescricao(rs.getString("descricao"));
				contato.setPreco(rs.getDouble("preco"));
								
				contatos.add(contato);
			}
			
			rs.close();
			stmt.close();
			return contatos;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public void altera(Produto produto){
		String sql = "update produtos set descricao=?, preco=? where codigo=?";
		
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, produto.getDescricao());
			stmt.setDouble(2, produto.getPreco());
			
			stmt.setLong(3, produto.getCodigo());
			stmt.execute();
			stmt.close();
		} catch (Exception e) {
			throw new RuntimeException(e); 
		}
	}
	
	public void remove(Produto produto){
		try {
			PreparedStatement stmt = connection.prepareStatement("delete from produtos where codigo=?");
			stmt.setLong(1, produto.getCodigo());
			stmt.execute();
			stmt.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
