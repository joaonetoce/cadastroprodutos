package br.com.fic.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.fic.dao.ProdutoDAO;
import br.com.fic.modelo.Produto;

public class Controle extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
				
		String operacao = request.getParameter("tipoRequisicao");
		
		if(operacao.equals("adiciona")){
			adiciona(request, response);
		}
		if(operacao.equals("altera")){
			altera(request, response);
		}
		if(operacao.equals("remove")){
			remove(request, response);
		}
	}
	
	public void adiciona(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		String descricao = request.getParameter("descricao");
		String preco = request.getParameter("preco");
		
		Produto produto = new Produto();
		produto.setDescricao(descricao);
		produto.setPreco(Double.parseDouble(preco));
		
		
		try {
			ProdutoDAO dao;
			dao = new ProdutoDAO();
			dao.adiciona(produto);
			RequestDispatcher rd = request.getRequestDispatcher("incluidosucesso.html");
			rd.forward(request, response);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			RequestDispatcher rd = request.getRequestDispatcher("erro.html");
			rd.forward(request, response);
			e.printStackTrace();
		}
	}
	
	public void altera(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String codigo = request.getParameter("codigo");
		String descricao = request.getParameter("descricao");
		String preco = request.getParameter("preco");
		
		Produto produto = new Produto();
		produto.setCodigo(Long.parseLong(codigo));
		produto.setDescricao(descricao);
		produto.setPreco(Double.parseDouble(preco));
		
		try {
			ProdutoDAO dao;
			dao = new ProdutoDAO();
			dao.altera(produto);
			RequestDispatcher rd = request.getRequestDispatcher("alteradosucesso.html");
			rd.forward(request, response);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			RequestDispatcher rd = request.getRequestDispatcher("erro.html");
			rd.forward(request, response);
			e.printStackTrace();
		}
	}
	
	public void remove(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
String codigo = request.getParameter("codigo");
		
		Produto produto = new Produto();
		produto.setCodigo(Long.parseLong(codigo));
		
		try {
			ProdutoDAO dao = new ProdutoDAO();
			dao.remove(produto);
			RequestDispatcher rd = request.getRequestDispatcher("removidosucesso.html");
			rd.forward(request, response);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			RequestDispatcher rd = request.getRequestDispatcher("erro.html");
			rd.forward(request, response);
			e.printStackTrace();
		}
	}
}
