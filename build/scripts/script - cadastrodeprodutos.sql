CREATE SCHEMA `produtosdb` ;

CREATE TABLE `produtosdb`.`produtos` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(45) NULL,
  `preco` DECIMAL(10,2) NULL,
  PRIMARY KEY (`codigo`));


